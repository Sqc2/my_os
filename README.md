# XeroLinux and Arch based

* **Site web:** https://sqc.go.yo.fr
* **Email:** qsoftware@protonmail.com
* **License:** GNU General Public License (version 3)
* **Contributeurs:** [....]

----

#### Description
L'idée m'est venue en découvrant une distribution Arch Based : XeroLinux. Malheureusement, suite au départ d'un développeur, XeroLinux était entrain de couler, seul la version KDE est toujours disponible est maintenue. Et c'est ainsi grâce à la magie des logiciels libres que j'ai découvert les divers outils et scrpits que l'équipe avait réaliser. --> **MON IDÉE :** Créer un remixe de XeroXFCE pour en faire une petite distribution desktop à mon goût.

#### Remerciements
* XeroLinux : https://xerolinux.xyz (voir aussi dépôt GIT : https://github.com/xerolinux)

### Autres
